<!DOCTYPE html>
<html>
<head>
    <title>File Upload Form</title>
</head>
<body>
<?php
// Check if the form was submitted
if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES["archivo"])){
    $num=count($_FILES["archivo"]["name"]);

    //Miramos que los archivos subidos no hayan tenido errores y lo movemos a la carpeta de subidas
    for($i=0;$i<$num;$i++){
      if($_FILES["archivo"]["error"][$i] == 0){
        move_uploaded_file($_FILES["archivo"]["tmp_name"][$i],"subidas/".$_FILES["archivo"]["name"][$i]);
      }
    }
}

//miramos los archivos de la carpeta subidas
$archivos=scandir("subidas");

echo "<h1>Archivos</h1>";

//Recorremos el array con los nombres de los archivos
for($i=2;$i<count($archivos);$i++){
  $archivo=$archivos[$i];
  echo "<p>".$archivo."</p>";
  //Si es una imagen la enseñamos
  if(strPos("subidas/".$archivo,".jpg")||strPos("subidas/".$archivo,".png")){
    echo "<img src='subidas/".$archivo."' height=300>";
  //Si no simplemente abrimos el archivo y leemos y mostramos su contenido linea a linea
  }else{
    $file = fopen("subidas/".$archivo, "r");
    while(!feof($file)) {
      echo fgets($file) . "<br>";
    }
    fclose($file);
  }
}
?> 
</body>
</html>