<!DOCTYPE html>
<html>
<head>
    <title>File Upload Form</title>
</head>
<body>
<?php
// Check if the form was submitted

//Guardamos los nombres con extension de la carpeta de subidas
$archivos=scandir("subidas");

echo "<h1>Archivos</h1>";
//Recuperamos el nombre del archivo a buscar del formulario
$busqueda=$_POST["archivo"];
$found=true;
//Miramos los nombres de los archivos para ver si coincide
for($i=2;$i<count($archivos)&&$found;$i++){
  $archivo=$archivos[$i];
  //En caso afirmativo mostramos el nombre y contenido del archivo
  if($archivo==$busqueda){
    echo "<p>".$archivo."</p>";
    $file = fopen("subidas/".$archivo, "r");
    while(!feof($file)) {
      echo fgets($file) . "<br>";
    }
    fclose($file);
    $found=false;
  }
}

//Si no se ha encontrado mostramos el mensaje correspondiente
if($found){
  echo "No se ha encontrado el archivo ".$busqueda;
}
?> 
</body>
</html>