<!DOCTYPE html>
<html>
<head>
    <title>File Upload Form</title>
</head>
<body>
<?php
// Check if the form was submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if file was uploaded without errors
    if(isset($_FILES["archivo"]) && $_FILES["archivo"]["error"] == 0){
      echo "<h1>Datos del archivo</h1>" .
      "<p>Nombre: " . $_FILES["archivo"]["name"] . "</p>" . 
      "<p>Tipo: " . $_FILES["archivo"]["type"] . "</p>" . 
      "<p>Tamaño: " . $_FILES["archivo"]["size"] . " bytes</p>" . 
      "<p>Ubicacion Temporal: " . $_FILES["archivo"]["tmp_name"] . "</p>" ;
    } else{
            echo "Error: There was a problem uploading your file. Please try again."; 
        }
    } else{
        echo "Error: " . $_FILES["archivo"]["error"];
    }
?> 
</body>
</html>