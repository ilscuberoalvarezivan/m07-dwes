<html>
  <head>
    <title>PHP Pildora 4 Eje 10</title>
  </head>
  <body>
    <?php 
    
    class CabeceraPagina{
      private $titulo;
      private $orientacion;
      private $colorLetra;
      private $colorFondo;

      public function inicializar($titu,$orie,$colLet,$colFon){
        $this->titulo=$titu;
        $this->orientacion=$orie;
        $this->colorLetra=$colLet;
        $this->colorFon=$colFon;
      }

      public function mostrar(){
        echo '<h1 style="color:' . $this->colorLetra . ';background-color:'. $this->colorFon .';text-align:'. $this->orientacion .'">'. $this->titulo .'</h1>';
      }

    }

    $cabesa=new CabeceraPagina();
    $cabesa->inicializar("MECArrones con tomatico","center","red","Yellow");
    $cabesa->mostrar();

     ?> 
  </body>
</html>