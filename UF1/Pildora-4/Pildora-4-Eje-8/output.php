<html>
  <head>
    <title>PHP Pildora 4 Eje 8 Output</title>
  </head>
  <body>
    <?php 
    
      class Empleado{
        private $nombre;
        private $sueldo;

        public function inicializar($nom,$sue){
          $this->nombre=$nom;
          $this->sueldo=$sue;
        }

        public function mostrar(){
          $frase;
          if($this->sueldo>3000){
            $frase=" ha de pagar impuestos.";
          }else{
            $frase=" no ha de pagar impuestos.";
          }
          echo $this->nombre . $frase;
        }
      }

      $nom=$_POST["nombre"];
      $sue=$_POST["sueldo"];
      $empleado=new Empleado();
      $empleado->inicializar($nom,$sue);
      $empleado->mostrar();
    
     ?> 
  </body>
</html>