<html>
  <head>
    <title>PHP Pildora 4 Eje 9</title>
  </head>
  <body>
    <?php 
    
    class Menu{
      private $lista;

      public function cargarOpcion($texto,$href){
        $this->lista[count($this->lista)]=array("texto"=>$texto,"href"=>$href);
      }

      public function mostrar(){
        echo "<h5>Menu</h5>";
        for($i=0;$i<count($this->lista);$i++){
          echo "<a href='".$this->lista[$i]["href"]."'>".$this->lista[$i]["texto"]."</a><br/>";
        }
      }
    }
    

    $menu=new Menu();

    $menu->cargarOpcion("Gundam Fandom","https://gundam.fandom.com/es/wiki/Portada");
    $menu->cargarOpcion("Gundam Wikipedia","https://es.wikipedia.org/wiki/Gundam");
    $menu->mostrar();
     ?> 
  </body>
</html>