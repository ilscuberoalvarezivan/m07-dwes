<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <?php 
    
    class Menu{

      private $titulo;
      private $align;
      private $color;
      private $bgcolor;

      //establece caracteristicas del menu
      function __construct($titulo="Titulo de ejemplo",$align="center",$color="black",$bgcolor="none"){
        $this->titulo=$titulo;
        $this->align=$align;
        $this->color=$color;
        $this->bgcolor=$bgcolor;
      }
      //Muestra el menu
      function graficar(){
        echo "<h1 style='text-align:" . $this->align . ";color:" . $this->color . ";background-color:" . $this->bgcolor . ";'>" . $this->titulo . "</h1>";
      }
    }

    $menu=new Menu("Macarrones","right","yellow","pink");
    $menu->graficar();

     ?> 
  </body>
</html>