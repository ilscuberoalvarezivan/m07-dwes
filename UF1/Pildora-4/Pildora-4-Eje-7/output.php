<html>
  <head>
    <title>PHP Pildora 4 Eje 7</title>
  </head>
  <body>
  <?php 
    
    function separar($lista){
      $nums;
      $pares=0;
      $impares=0;
      for($i=0;$i<count($lista);$i++){
        $num=$lista[$i];
        if($num%2==0){
          $nums[0][$pares]=$num;
          $pares++;
        }else{
          $nums[1][$impares]=$num;
          $impares++;
        }
      }

      return $nums;
    }

    $lista=explode(",",$_POST["nums"]);

    $nums=separar($lista);
    echo "Pares: " . implode(", ",$nums[0]) . "\nImpares: " . implode(", ",$nums[1]);
    
     ?> 
  </body>
</html>