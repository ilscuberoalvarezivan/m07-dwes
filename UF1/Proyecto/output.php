<!DOCTYPE html>
<html>
<head>
    <title>Proyecto Output</title>
</head>
<body>
<?php
//Array de usuarios y contraseñas
$users=array(
  array(
    "usr"=>"Paco",
    "pass"=>md5("1234")
  ),
  array(
    "usr"=>"Gundam",
    "pass"=>md5("Gandamu")
  ),
  array(
    "usr"=>"aaaaa",
    "pass"=>md5("bbbbb")
  )
);

//Recuperamos los datos del formulario
$usr=$_POST["user"];
$pass=$_POST["passwd"];

//Recorremos el array de usuarios para ver si coinciden usuario y contraseña
$incorrecto=true;
for($i=0;$i<count($users)&&$incorrecto;$i++){
  if($usr==$users[$i]["usr"]&&md5($pass)==$users[$i]["pass"]){
    $incorrecto=false;
  }
}

//Mostramos en la pagina si tiene acceso o no
if($incorrecto){
  echo "<p style='color:red'>Lo siento, no tienes acceso a esta página</p>";
}else{
  echo "<p style='color:green'>Bienvenido ".$usr."</p>";
}

?> 
</body>
</html>